let descriptions = [...document.querySelectorAll('.tabs-content__item')];
let tabsArr = [...document.querySelectorAll('.tabs-title')];

tabsArr.forEach(function (li) {
    li.addEventListener('click', changeActive);
});

function changeActive(event) {
    let active = document.querySelector('.active');
    active.classList.toggle('active');
    let elem = event.target;
    elem.classList.toggle('active');
    let i = tabsArr.indexOf(event.target);
    descriptions.forEach(function (item) {
        item.setAttribute('hidden', '');
    });
    descriptions[i].toggleAttribute('hidden');
}

